const fibonacci = require('./fibonacci');

describe('fibonacci', () => {
    test('should print and error message if the argument is not a number', () => {
        const expected = 'Error: the argument must be a number';
        const result = fibonacci('16');
        expect(expected).toBe(result);
    });

})