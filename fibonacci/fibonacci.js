function fibonacci(num) {

    if (typeof num !== 'number') {
        return 'Error: the argument must be a number';
    }

    let a = 0, b = 1, c, s = 1;
    console.log(a, b);
    for (i = 3; i <= num; i++) {
        c = a + b;
        console.log(c);
        s = s + c;
        a = b;
        b = c;
    }

    return num
}

function print(num) {
    for (let i = 0; i < num; i++) {
        console.log(`${i}: ${fibonacci(i)}`);
    }
}

print(50);


module.exports = fibonacci;