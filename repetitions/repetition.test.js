const repetitions = require('./repetition');

describe('repetitions', () => {
    test('should print and error message if the argument is not a string', () => {
        const expected = 'Error: the argument must be a string';
        const result = repetitions(16);
        expect(expected).toBe(result);
    });

})