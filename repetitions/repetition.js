function repetitions(text) {

    var texto = document.getElementById("text").value;
    texto = texto.replace(/\r?\n/g, " ");
    texto = texto.replace(/[ ]+/g, " ");
    texto = texto.replace(/^ /, "");
    texto = texto.replace(/ $/, "");
    var textoTroceado = texto.split(" ");
    var numeroPalabras = textoTroceado.length;
    alert(numeroPalabras);

    if (typeof texto !== 'string') {
        return 'Error: the argument must be a string';
    }

    return numeroPalabras;
}

function print(texto) {
    for (let i = 0; i < texto.length; i++) {
        console.log(`${i}: ${repetitions(i)}`);
    }
}

print('Hi how are things? how are you?. Are you a developer? I am also a developer');

module.exports = repetitions;